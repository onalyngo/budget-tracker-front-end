import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import View from '../../components/View';
import UserContext from '../../UserContext';
import moment from 'moment';

import PieChart from '../../components/PieChart'
import IncomePieChart from '../../components/IncomePieChart'
import ExpensePieChart from '../../components/ExpensePieChart'
import LineExample from '../../components/LineChart'

export default function index(){

	const {user, setUser} = useContext(UserContext);
	
	const [startDate, setStartDate] = useState(moment(Date.now()).format('YYYY-MM-DD'));
	const [endDate, setEndDate] = useState(moment(Date.now()).format('YYYY-MM-DD'));

	const [allRecords, setAllRecords] = useState([]);
	const [allCategories, setAllCategories] = useState([]);
	const [allRecordByCategories, setAllRecordByCategories] = useState([]);

	const [incomeCategories, setIncomeCategories] = useState([]);
	const [expenseCategories, setExpenseCategories] = useState([]);

	const [incomeRecords, setIncomeRecords] = useState([]);
	const [expenseRecords, setExpenseRecords] = useState([]);

	const [incomeByCategories, setIncomeByCategories] = useState([]);
	const [expenseByCategories, setExpenseByCategories] = useState([]);

	// For Line Chart
	const [dataLineChart, setDataLineChart] = useState([]);

	useEffect(() => {
		
		const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/details`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			// to get All Records
			let records = data.records;

			// For Line Chart
			let balance = 0;
			const dlc = data.records.map(record => {
					
				if(record.categoryType == "Income"){
					balance += record.amount
				} else {
					balance -= record.amount
				}
				return {
					amount: balance,
					date: record.date
				}
			})
			setDataLineChart(dlc)
			
			setAllRecords(records)
			setAllCategories(data.categories);

			// to get Income Records
			let filterIncome = records.filter(record => {
				return record.categoryType == "Income"
			})
			setIncomeRecords(filterIncome);

			// to get Expense Records
			let filterExpense = records.filter(record => {
				return record.categoryType == "Expense"
			})
			setExpenseRecords(filterExpense);

			// to get Income by Category
			let categories = data.categories;
			let incomeCat = categories.filter(category => {
				return category.type == "Income"
			})
			setIncomeCategories(incomeCat);

			// to get Income by Category
			let expenseCat = categories.filter(category => {
				return category.type == "Expense"
			})
			setExpenseCategories(expenseCat);
		})
	}, []);


	// For Income and Expense Breakdown Chart
	useEffect(() => {

			setAllRecordByCategories(
				allCategories.map(category => {

					let amount = 0;
					allRecords.forEach(record => {
						if(record.categoryName == category.name){
							amount += record.amount
						}
					})
					return {
						category: category.name,
						amount: amount
					}
				})
			)
	},[allCategories, allRecords])


	// For Income Pie Chart
	useEffect(() => {

		setIncomeByCategories(
			incomeCategories.map(category => {

				let amount = 0;
				incomeRecords.forEach(record => {
					if(record.categoryName == category.name){
						amount += record.amount
					}
				})
				return {
					category: category.name,
					amount: amount
				}
			})
		)
	},[incomeCategories, incomeRecords])

	// For Expense Pie Chart
	useEffect(() => {

			setExpenseByCategories(
				expenseCategories.map(category => {

					let amount = 0;
					expenseRecords.forEach(record => {
						if(record.categoryName == category.name){
							amount += record.amount
						}
					})
					return {
						category: category.name,
						amount: amount
					}
				})
			)
	},[expenseCategories, expenseRecords])



	return (
		<View title = {'Budget Tracker: Analytics'}>
			<Row className="justify-content-md-center">

				<Col sm={12} lg={10} className="mb-3">
					<PieChart categories={allRecordByCategories}/>
				</Col>

				<Col sm={12} lg={6} className="mb-3">
					<IncomePieChart categories={incomeByCategories} />
				</Col>
				
				<Col sm={12} lg={6} className="mb-3">
					<ExpensePieChart categories={expenseByCategories} />
				</Col>

				<Col sm={12} lg={10}>
					<LineExample dataLineChart={dataLineChart} />
				</Col>
			</Row>
		</View>
	)
}