import {  Row, Col } from 'react-bootstrap';


import View from '../../components/View';
import AddCategory from '../../components/AddCategory'
import ShowCategories from '../../components/ShowCategories'

export default function index(){
	return (
		<View title = {'Budget Tracker: Categories'}>
			<Row className="justify-content-center">
				<Col xs md="10" lg="6">
					
					<AddCategory />
					<ShowCategories />
				</Col>
			</Row>
		</View>
	)
}