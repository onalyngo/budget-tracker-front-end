import React, { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import moment from 'moment';
import { Alert, Row, Col, Card } from 'react-bootstrap';

import View from '../../components/View';
import { colorRandomizer } from '../../helpers';


export default function index(){

	const [chartLabels, setChartLabels] = useState([]);
	const [amounts, setAmounts] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [records, setRecords] = useState([]);


	const data = {
		labels: chartLabels,
		datasets: [{
			data: amounts,
			backgroundColor: bgColors,
			hoverBackground: bgColors
		}]
	}


	useEffect(() =>{

		let tempLabels = [];
        	records.forEach(record => {

        		if (!tempLabels.find(label => label === record.categoryName)) {
        			tempLabels.push(record.categoryName)
        		}
    		})

        	setChartLabels(tempLabels)
        	
        	setAmounts(
        		tempLabels.map(label =>{
	        		let total = 0
	        		records.forEach(record => {
	        			if(record.categoryName ===  label){
	        				total += record.amount 
	        			}
	        		})
	        		return total
        		})
        	)

        	setBgColors(records.map(() => `#${colorRandomizer()}`))

	},[ records ])


	useEffect(() =>{

		const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
		};
		
		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/details`, options)
		.then(res => res.json())
        .then(data => {
        	
        	let filteredRecords = data.records.map(record => {
				if( (moment(record.date).format('YYYY-MM-DD') >= moment(startDate).format('YYYY-MM-DD')) && (moment(record.date).format('YYYY-MM-DD') <= moment(endDate).format('YYYY-MM-DD')) ){
					return record
				} else {
					return null
				}
			})

			let finalRecords = filteredRecords.filter(record => record !== null)
				
			if(finalRecords.length > 0){
				setRecords(finalRecords)
			} else {
				setRecords([])
			}
        })
	},[ startDate, endDate ])


	return(
		<React.Fragment>
			<View title = {'Budget Tracker: Breakdown'}>
				<Row className="justify-content-md-center">
					<Col className="mb-3 text-center">
						<input type="date" onChange={(e) => setStartDate(e.target.value)}/>
						<input type="date" onChange={(e) => setEndDate(e.target.value)}/>
						{startDate !== '' && endDate !== '' && records.length > 0
							?
							<div>
								<Card className='py-5'>
									<h3 className='text-center'>Income and Expense Breakdown</h3>
									<Pie data={data} />
								</Card>
							</div>
							:
							<Alert variant="info">
								Please select a valid date
							</Alert>
						}
					</Col>
				</Row>
			</View>
		</React.Fragment>
	)
}