import React, { useState, useEffect } from 'react';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from '../components/NavBar';
import { Container } from 'react-bootstrap';
import { UserProvider } from '../UserContext';

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		id: null
	})
	const [records, setRecords] = useState([])

	useEffect(() => {
		
		const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/details`, options)
		.then(res => res.json())
		.then(userData => {
			
			if (typeof userData._id !== 'undefined') {
				setUser({
					id: userData._id
				})
				setRecords(userData.records)
			} else {
				setRecords([])
				setUser({
					id: null
				})
			}
		})

	}, [user.id])

	const unsetUser = () => {
		localStorage.clear();

		setUser({
			id: null
		})
	}


	return (
		<React.Fragment>
			<UserProvider value={{user, setUser, unsetUser, records, setRecords}}>
				<NavBar />
				<Container>
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</React.Fragment>
	)
}

export default MyApp
