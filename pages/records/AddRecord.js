import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Col, Card, Row } from 'react-bootstrap';
import Link from 'next/link';
import Router from 'next/router';
import Swal from 'sweetalert2';

import View from '../../components/View';


export default function AddRecord () {

	const addNewCategory = () => {
    	Router.push('/categories')
    }

	const viewRecord = () => {
        Router.push('/records')
    }

	const[categoryName, setCategoryName] = useState('')
	const[categoryType, setCategoryType] = useState('')
	const[description, setDescription] = useState('')
	const[amount, setAmount] = useState(0)
	const[isActive, setIsActive] = useState('')

	const addRecords = (e) => {
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/records`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json' 
			},
			body: JSON.stringify({
				categoryName: categoryName,
				categoryType: categoryType,
				description: description,
				amount: amount
			})
		})
		.then(res => res.json())
		.then(data =>{
			
			if(data === true){
				Swal.fire({
					title: 'Posted Transaction Successfully!',
					icon: 'success'
				})
				Router.push('/home')
			} else {
				Swal.fire(
					'Oops...',
					'Something went wrong!',
					'error'
				)
			}
		})

		setCategoryName('');
		setCategoryType('');
		setDescription('');
		setAmount(0);
	}

	useEffect(() => {
		
		if(categoryName !== '' && categoryType !== '' && amount !== 0){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ categoryName, categoryType, description, amount ])


	const [category, setCategory] = useState([]);

		// Get the user's categories when the component mounts
		useEffect(() => {

			const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
			};
			
			fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/details`, options)
			.then(res => res.json())
	        .then(data => {
	        	
	        	if (data._id){ // JWT validated
	        		setCategory(data.categories)
	        	} else { // JWT is invalid or non-existent
	        		setCategory([])
	        	}
	        })
		}, [])


	return(
		<React.Fragment>
			<View title = {'Add New Record'}>
				<Row className="justify-content-center">
					<Col xs md="10" lg="6">		
						<Card>
							<Card.Header className="text-center">Post New Transaction</Card.Header>
							<Card.Body>
								<Form onSubmit={ e => addRecords(e) }>

									<Form.Group controlId="categoryName">
										<Form.Control 
											as="select"
											value={categoryName}
											onChange={e => setCategoryName(e.target.value)}
											required>

											<option >Select category name</option>
											{category.map(category => {
												return(<option key={category._id} value={category.name}>{category.name}</option>)
											})}
										</Form.Control>
									</Form.Group>
							
									<Form.Group controlId="categoryType">
										
										<Form.Control 
											as="select" 
											value={categoryType} 
											onChange={e => setCategoryType(e.target.value)}
											required>
											<option value="">Select category type</option>
											<option value="Income">Income</option>
											<option value="Expense">Expense</option>
										</Form.Control>
									</Form.Group>
								
									<Form.Group controlId="description">
										<Form.Control 
											type="text"
											placeholder="Input description"
											value={description}
											onChange={e => setDescription(e.target.value)}
											required>
										</Form.Control>	
									</Form.Group>
								
									<Form.Group controlId="amount">	
										<Form.Control 
											type="number"
											placeholder="Input amount"
											value={amount}
											onChange={e => setAmount(e.target.value)}
											required>
										</Form.Control>
									</Form.Group>
									<React.Fragment>
									{ isActive
										?
										<Button block className="bg-warning" type="submit" id="submitBtn">
											Post Transaction
										</Button>
										:
										<Button block className="bg-danger" type="submit" id="submitBtn" disabled>
											Post Transaction
										</Button>
									}
									</React.Fragment>
									<React.Fragment>
									{ isActive
										?
										<Button block className="mt-3 bg-danger" onClick={addNewCategory} disabled>
											Add New Category
										</Button>
										:
										<Button block className="mt-3 bg-warning" onClick={addNewCategory}>
											Add New Category
										</Button>
									}
									</React.Fragment>
								</Form>
							</Card.Body>
						</Card>
				
						<div className="py-3">
							<Button block className="bg-success" onClick={viewRecord}>
								View Records History
							</Button>
						</div>
					</Col>
				</Row>
			</View>
		</React.Fragment>
	)
}