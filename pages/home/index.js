import { Row, Col} from 'react-bootstrap';
import { Container } from 'react-bootstrap';

import LottieAnimation from '../../components/Lottie';
import home from '../../components/lottie/dashboard.json';
import View from '../../components/View';
import UserDetails from '../../components/UserDetails'
import ShowAddRecord from '../../components/ShowAddRecord'

export default function Home(){
	return (
		<View title = {'Budget Tracker'}>
			<Container>
					<div className="card d-flex border-0">
						<div className="row d-flex justify-content-center">
							<div className="col-md-6 justify-content-center">
								<UserDetails />
							</div>
							<div className="col-md-6 col-sm-4 border-0">
								<Dashboard />
							</div>
						</div>
					</div>
					<ShowAddRecord />

			</Container>
		</View>
	)
}

const Dashboard = () => {

    return ( 
         <div className='dashboard'> 
           <LottieAnimation lotti={home} height={300} width={300} />
        </div>
    )
}
