import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import Swal from 'sweetalert2';
import LottieAnimation from '../../components/Lottie';
import home from '../../components/lottie/onlineregistration.json';
import View from '../../components/View';

export default function index(){
	return (
		<View title = {'Budget Tracker: Register'}>
			<div className="card d-flex border-0">
				<h3 className="card-header text-center border-0 bg-white mb-5">Register Now!</h3>
				<div className="row d-flex">
					<div className="col-md-6 text-center my-3">
						<RegisterAnimation />
					</div>
					<div className="col-md-6 col-sm-4">
						<RegisterForm />
					</div>
				</div>
			</div>
		</View>
	)
}


const RegisterAnimation = () => {

    return ( 
         <div className='registerAnimation'> 
           <LottieAnimation lotti={home} height={400} width={300} />
        </div>
    )
}


const RegisterForm = () => {

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);


	function registerUser(e) {
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/email-exists`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email				
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			if(data === false){
				fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users`, {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,
					})
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)

					if(data === true){
						Swal.fire({
							title: 'Registered Successfully!',
							icon: 'success'
						})
						Router.push('/')
					} else {
						Swal.fire(
							'Oops...',
							'Something went wrong!',
							'error'
						)
					}
				})
			} else {
				Swal.fire(
					'Email has been used.',
					'Try a different one.',
					'error'
				)
			}
		})
	
		setEmail('');
		setPassword1('');
		setPassword2('');

	}

	useEffect(() => {
		
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ email, password1, password2 ])


	return (
		<Card>
			<Card.Header className="text-center" border="light">Register</Card.Header>
			<Card.Body>
			<Form onSubmit={ e => registerUser(e) }>
				<Form.Row>
					<Col>
						<Form.Group controlId="firstName">
							<Form.Control 
								type="firstName"
								placeholder="Enter Your First Name"
								value={firstName}
								onChange={e => setFirstName(e.target.value)}
								required
							/>
						</Form.Group>
					</Col>
					
					<Col>
						<Form.Group>
							<Form.Control 
								type="lastName"
								placeholder="Enter Your Last Name"
								value={lastName}
								onChange={e => setLastName(e.target.value)}
								required
							/>
						</Form.Group>
					</Col>
				</Form.Row>

				<Form.Group controlId="userEmail">
					<Form.Control 
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>
			
				<Form.Group>
					<Form.Control 
						type="mobileNo"
						placeholder="11 digit mobile number"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>
				</Form.Group>
					
				
				<Form.Row>
					<Col>
						<Form.Group controlId="password1">
							<Form.Control 
								type="password"
								placeholder="Enter password"
								value={password1}
								onChange={e => setPassword1(e.target.value)}
								required
							/>
						</Form.Group>
					</Col>

					<Col>
						<Form.Group controlId="password2">
							<Form.Control 
								type="password"
								placeholder="Verify password"
								value={password2}
								onChange={e => setPassword2(e.target.value)}
								required
							/>
						</Form.Group>
					</Col>
				</Form.Row>
				
				{ isActive
					?
					<Button className="w-100 bg-primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button className="w-100 bg-danger" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
				}
			</Form>
			<p className="text-center py-3">
				Already registered? <Link href="/"><a>Login Now</a></Link>
			</p>
			</Card.Body>
		</Card>
	)
}