import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import { Alert, Row, Col } from 'react-bootstrap';

import View from '../../components/View';
import { colorRandomizer } from '../../helpers';

export default function trend(){

	const [chartLabels, setChartLabels] = useState([]);
	const [amounts, setAmounts] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [records, setRecords] = useState([]);

	const data = {
		labels: chartLabels,
		datasets: [{
			data: amounts,
			label: 'Balance',
			fill: false,
			lineTension: 0.1,
			backgroundColor: 'rgba(0, 0, 0, .5)',
			borderColor: `#${colorRandomizer()}`,
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle: 'miter',
			pointBorderColor: 'rgba(75,192,192,1)',
			pointBackgroundColor: '#fff',
			pointBorderWidth: 4,
			pointHoverRadius: 6,
			pointHoverBackgroundColor: 'rgba(75,192,192,1)',
			pointHoverBorderColor: 'rgba(220,220,220,1)',
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10				
		}]
	};

	let balance = 0;
	const dlc = records.map(record => {

		if(record.categoryType == "Income"){
			balance += record.amount
		} else {
			balance -= record.amount
		}
		return {
			amount: balance,
			date: record.date
		}
	})

	useEffect(() => {

		setChartLabels(dlc.map(element => moment(element.date).format("MMM DD 'YY")  ))

		setAmounts(dlc.map(element => element.amount))

	}, [records])


	useEffect(() =>{

		const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
		};
		
		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/details`, options)
		.then(res => res.json())
        .then(data => {
        	
        	let filteredRecords = data.records.map(record => {
				if( (moment(record.date).format('YYYY-MM-DD') >= moment(startDate).format('YYYY-MM-DD')) && (moment(record.date).format('YYYY-MM-DD') <= moment(endDate).format('YYYY-MM-DD')) ){
					return record
				} else {
					return null
				}
			})

			let finalRecords = filteredRecords.filter(record => record !== null)
				
			if(finalRecords.length > 0){
				setRecords(finalRecords)
			} else {
				setRecords([])
			}
        })
	},[ startDate, endDate ])


	return(
		<View title = {'Budget Tracker: Trend'}>
			<Row className="justify-content-md-center">
				<Col className="mb-3 text-center">
					<input type="date" onChange={(e) => setStartDate(e.target.value)}/>
					<input type="date" onChange={(e) => setEndDate(e.target.value)}/>
					{startDate !== '' && endDate !== '' && records.length > 0
						?
						<Line data={data} />
						:
						<Alert variant="info">
							Please select a valid date
						</Alert>
					}
				</Col>
			</Row>
		</View>
	)
}