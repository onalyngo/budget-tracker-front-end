import React, { useState, useEffect } from 'react';
import { Form, Button, Col, Card } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function AddCategory(){
	const[categoryName, setCategoryName] = useState('')
	const[categoryType, setCategoryType] = useState('')
	const[isActive, setIsActive] = useState('')

	const addCategory = (e) => {
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/categories`, {
			method: 'POST',
			headers: { 
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json' 
			},
			body: JSON.stringify({
				name: categoryName,
				type: categoryType
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data)
			if(data === true){
				Swal.fire({
					title: 'Category added successfully!',
					icon: 'success'
				})
				Router.push('/records/AddRecord')
			} else {
				Swal.fire(
					'Oops...',
					'Something went wrong!',
					'error'
				)
			}
		})

		setCategoryName('');
		setCategoryType('');
	}


	useEffect(() => {
		
		if(categoryName !== '' && categoryType !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ categoryName, categoryType ])

	return(
		<React.Fragment>
			<Card className="mb-3">
				<Card.Header className="text-center">Add Category</Card.Header>
				<Card.Body>
					<Form onSubmit={ e => addCategory(e) }>
						<Form.Row>
							<Col sm="6" md="5">
								<Form.Group controlId="categoryName">
									<Form.Control 
										type="text"
										placeholder="Input category name"
										value={categoryName}
										onChange={e => setCategoryName(e.target.value)}
										required
									/>
								</Form.Group>
							</Col>
							<Col sm="6" md="5">
								<Form.Group>
									<Form.Control 
										as="select" 
										value={categoryType} 
										onChange={e => setCategoryType(e.target.value)}
										required>
										<option value="">Select Type</option>
										<option value="Income">Income</option>
										<option value="Expense">Expense</option>
									</Form.Control>
								</Form.Group>
							</Col>

							<Col>
							{ isActive
								?
								<Button block className="bg-primary" type="submit" id="submitBtn">
									Add
								</Button>
								:
								<Button block className="bg-danger" type="submit" id="submitBtn" disabled>
									Add
								</Button>
							}
							</Col>

						</Form.Row>
					</Form>
				</Card.Body>
			</Card>
		</React.Fragment>
	)
}