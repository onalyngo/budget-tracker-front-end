import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { toNum } from '../helpers';


export default function UserDetails () {

	const [name, setName] = useState('');
	const [balance, setBalance] = useState('');
	const [income, setIncome] = useState('');
	const [expense, setExpense] = useState('');

	useEffect(() => {

		const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
		};

		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/details`, options)
		.then(res => res.json())
		.then(data => {
			
			setName(`${data.firstName}`)

			let records = data.records;

			let filterIncome = records.filter(record => {
				return record.categoryType == "Income"
			})
				
				let totalIncome = 0;
				filterIncome.map(income => {
					totalIncome += (income.amount)
				})
				setIncome(totalIncome);

			let filterExpense = records.filter(record => {
				return record.categoryType == "Expense"
			})
				
				let totalExpense = 0;
				filterExpense.map(expense => {
					totalExpense += expense.amount
				})
				setExpense(totalExpense);

			setBalance(totalIncome - totalExpense)
		})
		
	}, [])

	return(
		<Card className="column w-100 text-center mt-5 border-0">
			<Card.Body>
				<h2>
					Hi <em>{name}!</em>
				</h2>			
				
				<h4 className="py-3">
					Current Balance: <strong>Php {balance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</strong>
				</h4>
				
				<Row className="block justify-content-center py-3">
					<Col>
						<h5>
							Total Income: Php {income.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
						</h5>
					</Col>
					<Col className="text-danger">
						<h5>
							Total Spent: Php {expense.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
						</h5>
					</Col>
				</Row>
			</Card.Body>
		</Card>
	)

}