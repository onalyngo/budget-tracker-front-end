import React, { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { Card } from 'react-bootstrap';
import { colorRandomizer } from '../helpers';

export default function IncomePieChart({categories}) {
	// console.log(categories)

	const [categoryName, setCategoryName] = useState([]);
	const [amount, setAmount] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	useEffect(() => {

		if(categories.length > 0) {
			setCategoryName(categories.map(element => element.category));
			setAmount(categories.map(element => element.amount));

			setBgColors(categories.map(() => `#${colorRandomizer()}`))
		}

	}, [categories])


	const data = {
		labels: categoryName,
		datasets: [{
			data: amount,
			backgroundColor: bgColors,
			hoverBackgroundColor: bgColors
		}]
	}


    return (
    	<div>
	        <Card className='py-5'>
	      		<h3 className='text-center'>Income Breakdown</h3>
	        	<Pie data={data} />
	        </Card>
        </div>
    );
};